<?php
		echo 'Task#1:</br>';
		$n=0;
		while ($n<=100){

			if ($n%3==0) echo "$n ";
			$n++;

		}
		echo '</br></br>';

		echo 'Task#2:</br>';
		$n=10;
		$i=0;

			do{
				if ($i==0) echo "$i-это ноль</br>";
				elseif ($i%2==0) echo "$i -это четное число</br>";
				else echo "$i-это нечетное число</br>";
				$i++;
			} while($i<=$n);
		echo '</br></br>';

		echo 'Task#3:</br>';
		$n=9;
		for ($i=0;$i<=$n;print 	$i++." "){ }
		echo '</br></br>';

		echo 'Task#4:</br>';
		$region=array(
			"Московская область"=>array("Москва","Зеленоград","Клин"),
			"Ленинградская область"=>array("Санкт-Петербург","Всеволожск","Павловск","Кронштадт"),
			"Рязанская область"=>array("Рязань","Касимов","Скопин")
		);
		$moscow=implode(', ',$region["Московская область"]);
		$leningrad=implode(', ',$region["Ленинградская область"]);
		$ryazan=implode(', ',$region["Рязанская область"]);
		foreach ($region as $sKey => $cities)
		{

			echo(
				"<h2>$sKey:</h2>".
				"<ul>"
			);

				if ($sKey=="Московская область")
				echo("$moscow");
				elseif ($sKey=="Ленинградская область")
				echo("$leningrad");
				elseif ($sKey=="Рязанская область")
				echo("$ryazan");

			echo"</ul>";
		}
		echo '</br></br>';

		echo 'Task#5:</br>';

		foreach ($region as $sKey => $cities)
		{

			echo(
				"<h2>$sKey:</h2>".
				"<ul>"
			);

			foreach ($cities as $city)

			{
				if (mb_substr($city,0,1)=='К')
				echo $city;
			}

			echo"</ul>";
		}
		echo '</br></br>';

		echo 'Task#6:</br>';
		function rus2translit($str) {
			$converter = array(
				'а' => 'a',   'б' => 'b',   'в' => 'v',
				'г' => 'g',   'д' => 'd',   'е' => 'e',
				'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
				'и' => 'i',   'й' => 'y',   'к' => 'k',
				'л' => 'l',   'м' => 'm',   'н' => 'n',
				'о' => 'o',   'п' => 'p',   'р' => 'r',
				'с' => 's',   'т' => 't',   'у' => 'u',
				'ф' => 'f',   'х' => 'h',   'ц' => 'c',
				'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
				'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
				'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

				'А' => 'A',   'Б' => 'B',   'В' => 'V',
				'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
				'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
				'И' => 'I',   'Й' => 'Y',   'К' => 'K',
				'Л' => 'L',   'М' => 'M',   'Н' => 'N',
				'О' => 'O',   'П' => 'P',   'Р' => 'R',
				'С' => 'S',   'Т' => 'T',   'У' => 'U',
				'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
				'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
				'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
				'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
			);
			return strtr($str, $converter);
		}

		echo rus2translit('любовь');
		echo '</br></br>';

		echo 'Task#7:</br>';
		$str='Урок php номер 3';
		$str = str_replace(' ', '_', $str);
		echo $str;
		echo '</br></br>';

		echo 'Task#8:</br>';
		function rus2translit_replace($str)
		{
			$converter = array(
				'а' => 'a',   'б' => 'b',   'в' => 'v',
				'г' => 'g',   'д' => 'd',   'е' => 'e',
				'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
				'и' => 'i',   'й' => 'y',   'к' => 'k',
				'л' => 'l',   'м' => 'm',   'н' => 'n',
				'о' => 'o',   'п' => 'p',   'р' => 'r',
				'с' => 's',   'т' => 't',   'у' => 'u',
				'ф' => 'f',   'х' => 'h',   'ц' => 'c',
				'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
				'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
				'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

				'А' => 'A',   'Б' => 'B',   'В' => 'V',
				'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
				'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
				'И' => 'I',   'Й' => 'Y',   'К' => 'K',
				'Л' => 'L',   'М' => 'M',   'Н' => 'N',
				'О' => 'O',   'П' => 'P',   'Р' => 'R',
				'С' => 'S',   'Т' => 'T',   'У' => 'U',
				'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
				'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
				'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
				'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
			);
			$str = str_replace(' ', '_', $str);
			return strtr($str, $converter);
		}
		echo rus2translit_replace("Я такой крутой программист");






