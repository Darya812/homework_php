<?php

require_once "imgresize.php";

function upload_file($file)
{
    if ($file['name'] == '')
    {
        echo '<p>Файл не выбран!</p>';
        return;
    }
    if ($file['type'] != 'image/jpeg' && $file['type'] != 'image/png' && $file['type'] != 'image/gif')
    {
        echo '<p>Неподдерживаемый тип файла! '.$file['type'].'</p>';
        return;
    }
    if ($file['size'] > 2097152) //2Mb
    {
        echo '<p>Файл слишком большой!</p>';
        return;
    }
     if(!img_resize($file['tmp_name'],'images/preview/'.$file['name'],150,150))
    {
        echo '<p>Ошибка создания превью!</p>';
        return;
    }


    if (copy($file['tmp_name'], 'images/' . $file['name']))
        header("Location: index.php");
    else
        echo 'Ошибка загрузки файла';

}
?>
<html>
<head>
    <title>Галерея изображений</title>
</head>
<body>
<h2>Галерея изображений</h2>
<?php

$dir='images/preview/';
$images = scandir($dir);

    for ($i = 0; $i < count($images); $i++) {
        if (($images[$i] != ".") && ($images[$i] != "..")) {
            $path = $dir . $images[$i];
            echo "<a href=\"photo.php?name=" . $images[$i] . "\" target='_blank'><img  src=$path></a>";
        }
    }

?>
<h3>Загрузить изображение</h3>
<?php
if (isset($_FILES['file']))
{
    upload_file($_FILES['file']);
}
?>
<form method="post" enctype="multipart/form-data">
    <input type="file" name="file" />
    <input type="submit" value="Загрузить файл!" />
</form>

</html>