<?php
function upload_file($file)
{
    if ($file['name'] == '')
    {
        echo '<p>Файл не выбран!</p>';
        return;
    }
    if ($file['type'] != 'image/jpeg' && $file['type'] != 'image/png' && $file['type'] != 'image/gif')
    {
        echo '<p>Неподдерживаемый тип файла! '.$file['type'].'</p>';
        return;
    }
    if ($file['size'] > 2097152) //2Mb
    {
        echo '<p>Файл слишком большой!</p>';
        return;
    }
    if (copy($file['tmp_name'], 'img/' . $file['name']))
            header("Location: index.php");
        else
            echo 'Ошибка загрузки файла';

}
?>
<html>
<head>
    <title>Галерея изображений</title>
</head>
<body>
<h2>Галерея изображений</h2>
<?php
//находим все файловые пути, совпадающие с шаблоном
$images = glob('img/*');
foreach($images as $img){
    echo "<a href=".$img." target='_blank'><img  src=$img width=100></a>";
}

?>
<h3>Загрузить изображение</h3>
<?php
if (isset($_FILES['file']))
{
    upload_file($_FILES['file']);
}
?>
<form method="post" enctype="multipart/form-data">
    <input type="file" name="file" />
    <input type="submit" value="Загрузить файл!" />
</form>

</html>

       