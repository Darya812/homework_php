<?php
		$x=5;
		$y=10.5;
		$z=true;
		$a='hello';
		define('AGENT', '007');
		echo 'Task#1:'.'</br>';
		echo $x.'</br>';
		echo $y.'</br>';
		echo $z.'</br>';
		echo $a.'</br>';
		echo AGENT;
		echo '</br></br>';

		echo 'Task#2:'.'</br>';
		echo "$x".'</br>';
		echo "$y".'</br>';
		echo "$z".'</br>';
		echo "$a".'</br>';
		echo '</br></br>';

		echo 'Task#3:'.'</br>';
		echo '$x'.'</br>';
		echo '$y'.'</br>';
		echo '$z'.'</br>';
		echo '$a'.'</br>';
		echo '</br></br>';

		echo 'Task#4:'.'</br>';
		echo "«Славная осень! Здоровый, ядреный</br>";
		echo "Воздух усталые силы бодрит;</br>";
		echo "Лед неокрепший на речке студеной</br>";
		echo "Словно как тающий сахар лежит.»</br>";
		echo "<u>Н. А. Некрасов</u>";
		echo '</br></br>';

		echo 'Task#5:'.'</br>';
		echo "«Славная осень! Здоровый, ядреный</br>
		Воздух усталые силы бодрит;</br>
		Лед неокрепший на речке студеной</br>
		Словно как тающий сахар лежит.»</br>
		<u>Н. А. Некрасов</u>";
		echo '</br></br>';

		echo 'Task#6:'.'</br>';
		$x=10;
		$y='20 приветов';
		$z=$x+$y;
		echo  $z;
		echo '</br></br>';

		echo 'Task#7:'.'</br>';
		$a=true;
		$b=($a xor $a);
		echo $b.'</br>';

		$a=false;
		$b=($a xor $a);
		echo $b.'</br>';

		$a=true;
		$b=false;
		$c=($a xor $b);
		echo $c.'</br>';

		$a=false;
		$b=true;
		$c=($a xor $b);
		echo $c.'</br>';
		echo '</br></br>';

		echo 'Task#8:'.'</br>';
		$x = 10;
		$y = 15;
		echo $x.'  '.$y.'</br>';
		$y=$x+$y;
		$x=$y-$x;
		$y=$y-$x;
		echo $x.'  '.$y;
?>