<?php
/***
  функция создания уменьшенной копии :
  $src - имя исходного файла
  $dest - имя получившегося изображения
  $width, $height  - ширина и высота получившегося изображения
***/
header('Content-Type: text/html; charset=utf-8');
function img_resize($src, $dest, $width, $height) {
    if (!file_exists($src)) return false;
  // получим размеры исходного изображения
    $size = getimagesize($src);

    if ($size === false) return false;

    // Определяем исходный формат по MIME-информации, предоставленной
    // функцией getimagesize, и выбираем соответствующую формату
    // imagecreatefrom-функцию создания.
    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
    $icfunc = "imagecreatefrom" . $format;
    if (!function_exists($icfunc)) return false;
  //определение коэФфициентов сжатия
    $x_ratio = $width / $size[0];
    $y_ratio = $height / $size[1];

    $ratio       = min($x_ratio, $y_ratio);
    $use_x_ratio = ($x_ratio == $ratio);

    $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
    $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
    $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
    $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

    $isrc = $icfunc($src);
    // создадим пустое изображение по заданным размерам
    $idest = imagecreatetruecolor($width, $height);
  // зальём его белым цветом
    imagefill($idest, 0, 0,0xFFFFFF);
    // масштабируем изображение
    imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
      $new_width, $new_height, $size[0], $size[1]);
  //сохраняем получившееся изобрадение в файл
    imagejpeg($idest, $dest);

    imagedestroy($isrc);
    imagedestroy($idest);

    return true;
}

function upload_file($file)
{
    if ($file['name'] == '')
    {
        echo '<p>Файл не выбран!</p>';
        return;
    }
    if ($file['type'] != 'image/jpeg' && $file['type'] != 'image/png' && $file['type'] != 'image/gif')
    {
        echo '<p>Неподдерживаемый тип файла! '.$file['type'].'</p>';
        return;
    }
    if ($file['size'] > 2097152) //2Mb
    {
        echo '<p>Файл слишком большой!</p>';
        return;
    }
    if(!img_resize($file['tmp_name'],'images/preview/'.$file['name'],150,150))
    {
        echo '<p>Ошибка создания превью!</p>';
        return;
    }
    if(copy($file['tmp_name'], 'images/'.$file['name'])) {
        if (mysql_query("replace into `gallery` values('".mysql_real_escape_string($file['name'])."',0);")) {
            mysql_close();
            header("Location: index.php");
            return;
        }
        echo '<p>Ошибка БД</p>';
        unlink('images/'.$file['name']);
        unlink('images/preview/'.$file['name']);
    }
    else
        echo '<p>Ошибка загрузки файла</p>';
}

function edit()
{
    if (isset($_GET['name'])) {
        $result = mysql_query('SELECT * FROM `gallery` where `name` = \'' . mysql_real_escape_string($_GET['name']) . '\';');
        if (!$result)
            die ('Не удалось найти картинку: ' . mysql_error());
        if ((isset($_POST['title']))&&(isset($_POST['alt'])))
        {
            if ($row = mysql_fetch_assoc($result)) {
                mysql_query('update `gallery` set `title` = \''.$_POST['title'].'\', `alt`=\''.$_POST['alt'].'\'where `name`=\'' . mysql_real_escape_string($_GET['name']) . '\';');
                mysql_close();
                header('Location:index.php');
                return;
            }
        }
    } else echo "<p>Ошибка</p>";

    return true;
}

