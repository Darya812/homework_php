<?php

require_once "config.php";
require_once "imgresize.php";

function upload_file($file)
{
    if ($file['name'] == '')
    {
        echo '<p>Файл не выбран!</p>';
        return;
    }
    if ($file['type'] != 'image/jpeg' && $file['type'] != 'image/png' && $file['type'] != 'image/gif')
    {
        echo '<p>Неподдерживаемый тип файла! '.$file['type'].'</p>';
        return;
    }
    if ($file['size'] > 2097152) //2Mb
    {
        echo '<p>Файл слишком большой!</p>';
        return;
    }
    if(!img_resize($file['tmp_name'],'images/preview/'.$file['name'],150,150))
    {
        echo '<p>Ошибка создания превью!</p>';
        return;
    }
    if(copy($file['tmp_name'], 'images/'.$file['name'])) {
        if (mysql_query("replace into `gallery` values('".mysql_real_escape_string($file['name'])."',0);")) {
            mysql_close();
            header("Location: index.php");
            return;
        }
        echo '<p>Ошибка БД</p>';
        unlink('images/'.$file['name']);
        unlink('images/preview/'.$file['name']);
    }
    else
        echo '<p>Ошибка загрузки файла</p>';
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Галерея</title>
</head>
<body>
<h1>Галерея</h1>
<?php
if (isset($_FILES['file']))
{
    upload_file($_FILES['file']);
}
?>
<?php
$result = mysql_query('SELECT * FROM `gallery` order by `views` desc, `name` asc');

while($row = mysql_fetch_assoc($result))

    echo "<div style='display:inline-block'><a href = \"photo.php?name=".$row['name']."\" target=\"_blank\"><img src = \"".LOCATION_PATH."images/preview/".$row['name']."\"></a><div style='display:block;text-align: center;'>(Просмотров:".$row['views'].")</div></div>";

mysql_close();
?>

<form method="post" enctype="multipart/form-data">
    <input type="file" name="file" />
    <input type="submit" value="Загрузить файл!" />
</form>
</body>
</html>