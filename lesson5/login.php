<html>
<head>
    <title>Вход на сайт</title>
    <form action="" method="post">
        Логин:<br/>
        <input type="text" name="username"/>

        <input type="checkbox" name="remember"/> Запомнить меня
        <br/>
        <input type="submit" value="Войти"/>
    </form>
</head>
</html>
<?php
header('Content-Type: text/html; charset=utf-8');
function Login($username, $remember)
{

    if ($username == '')
        return false;
// Запоминаем имя в сессии
    $_SESSION['username'] = $username;
// и в cookies, если пользователь пожелал запомнить его (на неделю).
    if ($remember)
        setcookie('username', $username, time() + 3600 * 24 * 7);
// Успешная авторизация.
    return true;
}

function Logout()
{ // Удаляем куки(делаем устаревшими).
    setcookie('username' , '', time()- 1);
// Сброс сессии.
unset($_SESSION['username']);
}

session_start();
$enter_site = false;
// Попадая на страницу login.php, авторизация сбрасывается.
Logout();
// Если массив POST не пуст, значит, обрабатываем отправку формы.
if (count($_POST) > 0)
    $enter_site = Login($_POST['username'], $_POST['remember'] == 'on');
// Если авторизация пройдена, переадресуем пользователя

if ($enter_site)
{
    header("Location:a.php");
    exit();
}
?>
